<?php 
require_once('model/AdministrateurModel.php');
require_once('model/EtudiantModel.php');
require_once('model/ToutLeMondeModel.php');

class Administrateur
{
	public $etudiantM ;
	public $adminM ;
	public $toutLeMondeM ;

	function __construct()
	{
		// Création des objets
		$this->etudiantM = new EtudiantModele();
		$this->adminM = new AdministrateurModele();
		$this->toutLeMondeM = new ToutLeMondeModele();
	}

	function ouvrirAdministrateur()
	{
		//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;
		$email ="";
		$password="";
		$infoUser = array();

		//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

		//INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		 
    	if (!isset($_POST['email']) and !isset($_POST['password'])) 
		{
			if (!isset($_SESSION['email']) and !isset($_SESSION['password'])) 
			{
				$redirection = "acceuil";
				throw new Exception('Votre session a expirée ');
			}
			else
			{
				$email = $_SESSION['email'];
				$password = $_SESSION['password'];
			}
		}
		else
		{
			$email = $_POST['email'];
			$password = $_POST['password'];
		}
		
    	$listInfo = $this->adminM->checkInfoAdmin($email, $password); // Appel d'une fonction de cet objet
    	if ($listInfo != null) 
    	{
    		$row = $listInfo;
			$infoUser = array('email' => $email, 'password'=>$password );

			$list = $this->adminM->getReservationEnCours(); // Appel d'une fonction de cet objet
			require('view/administrateur/administrateurView.php');

			return $infoUser;
		}
		else
		{
			$redirection = "acceuil";
            throw new Exception('Email ou mot de passe incorrect ! Veuillez vous reconnecter si vous avez modifié vos informations personnelles');
			return null; //meme si inutile
		}

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
	}

	function ouvrirAdministrateurC()
	{
		//echo "<script>alert('Email:  ou mdp: ');</script>";
		//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;
		$email ="";
		$password="";
		$infoUser = array();

		//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

		//INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
    	if (!isset($_POST['email']) and !isset($_POST['password'])) 
		{
			if (!isset($_SESSION['email']) and !isset($_SESSION['password'])) 
			{
				$redirection = "acceuil";
				throw new Exception('Votre session a expirée ');
			}
			else
			{
				$email = $_SESSION['email'];
				$password = $_SESSION['password'];
			}
		}
		else
		{
			$email = $_POST['email'];
			$password = $_POST['password'];
		}
		
    	$list = $this->adminM->checkInfoAdmin($email, $password); // Appel d'une fonction de cet objet
    	if ($list != null) {
    		$row = $list;
			// throw new Exception('good: email:'.$row['EtuEmail'].'mot de passe: '.$row['EtuMotDePasse']);
			$infoUser = array('email' => $email, 'password'=>$password );
			// require('view/administrateur/administrateurView.php');

			return $infoUser;
		}
		else
		{
			$redirection = "acceuil";
            throw new Exception('Email ou mot de passe incorrect');
			return null; //meme si inutile
		}

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
	}

	function ouvrirDetailMessageEtu(): void 
	{
		//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;
		 
		//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
    	$list = $this->adminM->getDetailMessageEtu($_GET['EventCode']); // Appel d'une fonction de cet objet
    	if ($list != null) {
    		$row = $list;
			require('view/administrateur/detailMessageEtuView.php');
		}
		else
		{
			$redirection = "detailMessageEtu";
            throw new Exception('Cette réservation n\'existe pas !');
		}

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
	}

	//Ajout des fonctions ouvrirM et ouvrirMesEtu by Syntiche
	function ouvrirMessageEnv(): void
        {
        	//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
        	
            global $reservation; // permet d'accer  aux variables de portée globale
			global $administrateur;
			global $redirection;
			 
			//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

            //INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
            
	    	$list = $this->adminM->getMessageEnv(); // Appel d'une fonction de cet objet
			require('view/administrateur/MessageEnvView.php');
	    	
	    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        }

        function ouvrirDetailMessageEnv(): void 
		{
			//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
			
			global $reservation; // permet d'accer  aux variables de portée globale
			global $administrateur;
			global $redirection;
			 
			//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
			
			//INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
	    	$list = $this->adminM->getDetailMessageEnv($_GET['MessCode']); // Appel d'une fonction de cet objet
	    	if ($list == true) {
	    		$row = $list;
				require('view/administrateur/detailMessageEnvView.php');
			}
			else
			{
				$redirection = "MessageEnv";
                throw new Exception('Ce message n\'existe pas !');
			}
	    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		}

		function ouvrirFormulMesEtu(): void
        {
        	//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
        	
            global $reservation; // permet d'accer  aux variables de portée globale
			global $administrateur;
			global $redirection;
			 
            //FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            
            //INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
            
	    	/*$list = $etudiantM->getist();*/ // Appel d'une fonction de cet objet

	    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

            
            require('view/administrateur/MessageEnvView.php');
        }

        function ouvrirEnvMessage(): void
        {
        	//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
        	
            global $reservation; // permet d'accer  aux variables de portée globale
			global $administrateur;
			global $redirection;
			 
            //FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            
            //INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
            
	    	$list = $this->adminM->getInfoEtu($_GET['EtuCode']); // Appel d'une fonction de cet objet
	    	if ($list != null) {
	    		$row = $list;
				// throw new Exception('good: email:'.$row['EtuEmail'].'mot de passe: '.$row['EtuMotDePasse']);
				require('view/administrateur/envMessageView.php');
			}
			else
			{
				$redirection = "administrateur";
                throw new Exception('Cette réservation n\'existe pas !');
			}
	    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        }

        function ouvrirRetourReservation(): void
        {
        	//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
        	
            global $reservation; // permet d'accer  aux variables de portée globale
			global $administrateur;
			global $redirection;
			 
            //FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            
            //INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
            
	    	$list = $this->adminM->getReservationRepondu(); // Appel d'une fonction de cet objet

            require('view/administrateur/retourReservationView.php');
	    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        }

        function ouvrirDetailMessageRepondu(): void 
		{
			//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
			
			global $reservation; // permet d'accer  aux variables de portée globale
			global $administrateur;
			global $redirection;
			 
			//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
			
			//INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
	    	$list = $this->adminM->getDetailMessageRepondu($_GET['EventCode']); // Appel d'une fonction de cet objet
	    	if ($list != null) {
	    		$row = $list;
				// throw new Exception('good: email:'.$row['EtuEmail'].'mot de passe: '.$row['EtuMotDePasse']);
				require('view/administrateur/detailMessageReponduView.php');
			}
			else
			{
				$redirection="retourReservation";
				throw new Exception('Cette réservation n\'existe pas !');
			}

	    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
			
		}

		function ouvrirModifierProfil(): void 
		{
			//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
			
			global $reservation; // permet d'accer  aux variables de portée globale
			global $administrateur;
			global $redirection;
			 
			//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
			
			//INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
	    	$list = $this->adminM->getInfoAdmin($_SESSION['email']); // Appel d'une fonction de cet objet
	    	if ($list != null) {
	    		$row = $list;
				// throw new Exception('good: email:'.$row['EtuEmail'].'mot de passe: '.$row['EtuMotDePasse']);
				require('view/administrateur/modifierProfilView.php');
			}
			else
			{
				$redirection = "administrateur";
                throw new Exception('Cet administrateur n\'existe pas !');
			}

	    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
			
		}

        function setReponseReservation(): void
        {
        	//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
        	
            global $reservation; // permet d'accer  aux variables de portée globale
			global $administrateur;
			global $redirection;
			 
            //FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            
            //INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
            
	    	$list = $this->adminM->setReponseEvenement($_GET['EventCode'], $_GET['reponse']); // Appel d'une fonction de cet objet
	    	if ($list != null) {
	    		$row = $list;
	    		header('Location: index.php?action=retourReservation');
			}
			else
			{
				$redirection = "administrateur";
                throw new Exception('Un problème est survenu lors de l\'envoie du formulaire !');
			}

	    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        }

        function setProfilAdmin(): void
        {
        	//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
        	
            global $reservation; // permet d'accer  aux variables de portée globale
			global $administrateur;
			global $redirection;
			 
            //FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            
            //INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
            
	    	$list = $this->adminM->setProfilAdmin($_POST['nom'], $_POST['prenom'], $_POST['email'], $_SESSION['email']); // Appel d'une fonction de cet objet
	    	if ($list != null) {
	    		$row = $list;
	    		$_SESSION['email']= $_POST['email'];
	    		echo '<script>alert("Les données on été modifiées avec succès");</script>';
	    		echo "<script>window.location='index.php?action=modifierProfilAdmin';</script>";
			}
			else
			{
				$redirection = "administrateur";
                throw new Exception('Un problème est survenu lors de l\'envoie du formulaire !');
			}

	    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        }

        function addMessageFromAdmin(): void
        {
        	//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
        	
            global $reservation; // permet d'accer  aux variables de portée globale
			global $administrateur;
			global $redirection;
			 
            //FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            
            //INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
            
	    	$list = $this->adminM->addMessageAdmin($_GET['EtuCode'], $_GET['EventCode'], $_POST['objet'], $_POST['contenu']); // Appel d'une fonction de cet objet
	    	if ($list == true) {
	    		header('Location: index.php?action=MessageEnv');
			}
			else
			{
				$redirection = "administrateur";
                throw new Exception('Un problème est survenu lors de l\'envoie du formulaire !');
			}

	    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        }
}