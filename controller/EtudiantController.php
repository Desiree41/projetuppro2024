<?php 
require_once('model/AdministrateurModel.php');
require_once('model/EtudiantModel.php');
require_once('model/ToutLeMondeModel.php');

class Etudiant
{
	public $etudiantM ;
	public $adminM ;
	public $toutLeMondeM ;

	function __construct()
	{
		// Création des objets
		$this->etudiantM = new EtudiantModele();
		$this->adminM = new AdministrateurModele();
		$this->toutLeMondeM = new ToutLeMondeModele();
	}

	function ouvrirReservation()
	{
		//REMPLISSAGE DES VARIABLES DE LIEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;
		$email ="";
		$password="";
		$infoUser = array();
		
		//FIN REMPLISSAGES DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: 
		if (!isset($_POST['email']) and !isset($_POST['password'])) 
		{
			if (!isset($_SESSION['email']) and !isset($_SESSION['password'])) 
			{
				$redirection = "acceuil";
				throw new Exception('Votre session a expirée ');
			}
			else
			{
				$email = $_SESSION['email'];
				$password = $_SESSION['password'];
			}
		}
		else
		{
			$email = $_POST['email'];
			$password = $_POST['password'];
		}
		
		
    	$list = $this->etudiantM->checkInfoEtu($email, $password); // Appel d'une fonction de cet objet
    	if ($list != null) {
    		$row = $list;
			// throw new Exception('good: email:'.$row['EtuEmail'].'mot de passe: '.$row['EtuMotDePasse']);
			$infoUser = array('email' => $email, 'password'=>$password );
			require('view/etudiant/reservationView.php');

			return $infoUser;
		}
		else
		{
			$redirection = "acceuil";
            throw new Exception('Email ou mot de passe incorrect !');
			return null; //meme si inutile
		}

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
	}

	function ouvrirReservationC()
	{
		//REMPLISSAGE DES VARIABLES DE LIEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;
		$email ="";
		$password="";
		$infoUser = array();
		
		//FIN REMPLISSAGES DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: 
		if (!isset($_POST['email']) and !isset($_POST['password'])) 
		{
			if (!isset($_SESSION['email']) and !isset($_SESSION['password'])) 
			{
				$redirection = "acceuil";
                throw new Exception('Votre session a expirée ');
			}
			else
			{
				$email = $_SESSION['email'];
				$password = $_SESSION['password'];
			}
		}
		else
		{
			$email = $_POST['email'];
			$password = $_POST['password'];
		}
		
		
    	$list = $this->etudiantM->checkInfoEtu($email, $password); // Appel d'une fonction de cet objet
    	if ($list != null) {
    		$row = $list;
			// throw new Exception('good: email:'.$row['EtuEmail'].'mot de passe: '.$row['EtuMotDePasse']);
			$infoUser = array('email' => $email, 'password'=>$password );
			
			return $infoUser;
		}
		else
		{
			$redirection = "acceuil";
            throw new Exception('Email ou mot de passe incorrect');
			return null; //meme si inutile
		}

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
	}

	function ouvrirboiteDeReceptionEtu(): void
	{
		//REMPLISSAGE DES VARIABLES DE LIEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;

		//FIN REMPLISSAGES DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: 
    	$list = $this->etudiantM->getReservationRepondu($_SESSION['email']); // Appel d'une fonction de cet objet
		require('view/etudiant/boiteDeReceptionEtuView.php');

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
	}

	function ouvrirDetailMessageRetour(): void
	{
		//REMPLISSAGE DES VARIABLES DE LIEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;

		//FIN REMPLISSAGES DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: 
    	$list = $this->etudiantM->getDetailMessageRetour($_GET['EventCode']); // Appel d'une fonction de cet objet
    	if ($list != null) {
    		$row = $list;
			// throw new Exception('good: email:'.$row['EtuEmail'].'mot de passe: '.$row['EtuMotDePasse']);
			require('view/etudiant/detailMessageRetourView.php');
		}
		else
		{
			$redirection = "boiteDeReceptionEtu";
            throw new Exception('Cette réservation n\'existe pas !');
		}

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
	}

	function ouvrirReservationEffectue(): void
	{
		//REMPLISSAGE DES VARIABLES DE LIEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;

		//FIN REMPLISSAGES DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: 
    	$list = $this->etudiantM->getReservationEffectue($_SESSION['email']); // Appel d'une fonction de cet objet
		require('view/etudiant/reservationEffectueView.php');

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
		
	}

	function ouvrirMessageRecu(): void
	{
		//REMPLISSAGE DES VARIABLES DE LIEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;

		//FIN REMPLISSAGES DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: 
    	$list = $this->etudiantM->getMessageRecu($_SESSION['email']); // Appel d'une fonction de cet objet
		require('view/etudiant/messageRecuView.php');

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
		
	}

	function ouvrirDetailMessageRecu(): void
	{
		//REMPLISSAGE DES VARIABLES DE LIEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;
		// global redirection:

		//FIN REMPLISSAGES DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: 
    	$list = $this->etudiantM->getDetailMessageRecu($_GET['MessCode']); // Appel d'une fonction de cet objet
    	if ($list == true) {
    		$row = $list;
			require('view/etudiant/detailMessageRecuView.php');
		}
		else
		{
			$redirection = "messageRecu";
            throw new Exception('Ce message n\'existe pas !');
		}

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
	}


	public function addReservation()
    {
        //REMPLISSAGE DES VARIABLES DE LIEN ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        
        global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		global $redirection;

        //FIN REMPLISSAGES DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        $infosfichier = pathinfo($_FILES['EventPhoto']['name']);
        
        
        //################################### INTERACTION AVEC LA BD ###################################
        $date = date("YmdHis");
		$nomImage = basename($_FILES['EventPhoto']['name']) ;
		$nomImage=$date.'_'.$nomImage;
		$taille = filesize($_FILES['EventPhoto']['tmp_name']);
		$tailleMax= 2000000;

		if ($_FILES['EventPhoto']['error'] == 0)
        {
			$extension_upload = $infosfichier['extension'];
			$extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png', '');

			if (in_array($extension_upload, $extensions_autorisees))
			{
				$upload = 'public/image/evenement/'.$nomImage;
		  		move_uploaded_file($_FILES['EventPhoto']['tmp_name'], $upload);

		  		//EVENEMENT :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		        $list = $this->etudiantM->addEvent($_POST['EventNom'], $_POST['EventDate'], $_POST['EventAccess'], $_POST['EventDes'], $_POST['EventType'], $nomImage);

		        $EventCode = 0;
		        $list1 = $this->etudiantM->getEventCode();
		        if ($list1 == true) 
		        {
		            $row1 = $list1;
		            $EventCode = $row1['lastEventCode'];
		        } else {
		        	$redirection = "reservation";
		            throw new Exception('Impossible de récupérer le code de l\'èvenement');
		        }
				
		        //ESPACE ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		        $list = $this->etudiantM->addEspace($_POST['EspNom'], $_POST['EventLoc'], $_POST['EventCat']);
		        
		        $EspCode = 0;
		        $list = $this->etudiantM->getEspCode();
		        if ($list != null) {
		            $row = $list;
		            $EspCode = $row['lastEspCode'];
		        } else {
		        	$redirection = "reservation";
		            throw new Exception('Impossible de récupérer le code de l\'espace');
		        }

		        //CONCERNER ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		        $list4 = $this->etudiantM->addConcerner($EventCode, $EspCode);

		        $list5 = $this->etudiantM->getEtuCode($_SESSION['email']);
		        if ($list5 != null) {
		            $row5 = $list5;
		            $EtuCode = $row5['EtuCode'];
		        } else {
		        	$redirection = "reservation";
		            throw new Exception('Impossible de récupérer le code de l\'étudiant');
		        }

		        //FAIRE RESERVATION ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		        $list6 = $this->etudiantM->AddFaireReservation($EventCode, $EtuCode);

        		header("Location: index.php?action=reservationEffectue");
			}
			else
			{
				$redirection="reservation";
                throw new Exception('Veillez choisir un image !');
			}	
        }
        elseif ($_FILES['EventPhoto']['error'] != 0 and !isset($infosfichier['extension'])) 
        {
        	$upload = 'public/image/evenement/' . $nomImage;
	  		move_uploaded_file($_FILES['EventPhoto']['tmp_name'], $upload);

	  		//EVENEMENT :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	        $list = $this->etudiantM->addEvent($_POST['EventNom'], $_POST['EventDate'], $_POST['EventAccess'], $_POST['EventDes'], $_POST['EventType'], $nomImage);

	        $EventCode = 0;
	        $list1 = $this->etudiantM->getEventCode();
	        if ($list1 == true) 
	        {
	            $row1 = $list1;
	            $EventCode = $row1['lastEventCode'];
	        } else {
	        	$redirection = "reservation";
	            throw new Exception('Impossible de récupérer le code de l\'èvenement');
	        }
			
	        //ESPACE ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	        $list = $this->etudiantM->addEspace($_POST['EspNom'], $_POST['EventLoc'], $_POST['EventCat']);
	        
	        $EspCode = 0;
	        $list = $this->etudiantM->getEspCode();
	        if ($list != null) {
	            $row = $list;
	            $EspCode = $row['lastEspCode'];
	        } else {
	        	$redirection = "reservation";
	            throw new Exception('Impossible de récupérer le code de l\'espace');
	        }

	        //CONCERNER ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	        $list4 = $this->etudiantM->addConcerner($EventCode, $EspCode);

	        $list5 = $this->etudiantM->getEtuCode($_SESSION['email']);
	        if ($list5 != null) {
	            $row5 = $list5;
	            $EtuCode = $row5['EtuCode'];
	        } else {
	        	$redirection = "reservation";
	            throw new Exception('Impossible de récupérer le code de l\'étudiant');
	        }

	        //FAIRE RESERVATION ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	        $list6 = $this->etudiantM->AddFaireReservation($EventCode, $EtuCode);

    		header("Location: index.php?action=reservationEffectue");
        }
        else
        {
            $redirection="reservation";
            throw new Exception('Veillez choisir un image de moins de 2 mo !');
        }
        

        //################################### ! INTERACTION AVEC LA BD ###################################
        
    }

}

