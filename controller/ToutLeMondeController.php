<?php 
require_once('model/AdministrateurModel.php');
require_once('model/EtudiantModel.php');
require_once('model/ToutLeMondeModel.php');

class ToutLeMonde
{
	public $etudiantM ;
	public $adminM ;
	public $toutLeMondeM ;

	function __construct()
	{
		// Création des objets
		$this->etudiantM = new EtudiantModele();
		$this->adminM = new AdministrateurModele();
		$this->toutLeMondeM = new ToutLeMondeModele();
	}

	function ouvrirAcceuil(): void
	{
		//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		ob_start(); 
			echo '<a href="index.php?action=evenement">Evènement</a>';
		$evenement= ob_get_clean();

		//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		 
    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  	

		require('view/toutLeMonde/acceuilView.php');
	}
	
	function ouvrirContact(): void
	{
		//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		ob_start(); 
			echo '<a href="index.php?action=evenement">Evènement</a>';
		$evenement= ob_get_clean();

		//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		require('view/toutLeMonde/contactView.php');
	}

	function ouvrirAProposDeNous(): void
	{
		//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		ob_start(); 
			echo '<a href="index.php?action=evenement">Evènement</a>';
		$evenement= ob_get_clean();

		//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		 
    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  
    	
		require('view/toutLeMonde/aProposDeNousView.php');
	}

	function ouvrirEvenement(): void
	{
		//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;
		ob_start(); 
			echo '<a href="index.php?action=evenement">Evènement</a>';
		$evenement= ob_get_clean();

		//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//INTERACTION AVEC LA BD ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		 
    	$list = $this->toutLeMondeM->getEvent(); // Appel d'une fonction de cet objet
		require('view/toutLeMonde/evenementView.php');

    	//FIN INTERACTION AVEC LA BD $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  
		
	}

	function sendEmail(): void
	{
		//REMPLISSAGE DES VARIABLES DE LIEN :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
		global $reservation; // permet d'accer  aux variables de portée globale
		global $administrateur;

		//FIN REMPLISSAGE DES VARIABLES DE LIENS $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		//ENVOIE D'EMAIL ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		 
		require('public/traitement/sendEmail.php');
		require('view/toutLeMonde/contactView.php');

    	//FIN ENVOIE D'EMAIL $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  
		
	}



}

