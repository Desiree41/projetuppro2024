<?php 
class template
{

    //SELECTION
    public function getReservationEffectue() // Sans insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->query('SELECT * FROM Evenements');// on execute la requet
        $result = $query->fetchAll(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getReservationEffectue2($code) // Avec insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('SELECT * FROM Evenements WHERE eventCode = ?');// on prépare la requete
        $query->execute(array($code)); // on execute la requete
        $result = $query->fetchAll(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getReservationEffectue3($code) // Avec insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('SELECT * FROM Evenements WHERE eventEstAccepte = ?');// on prépare la requete
        
        $query->bindValue('1', $EtuCode, PDO::PARAM_BOOL); // PARAM obligatoire pour boolean, bigint, limit(req sql)

        $result = $query->fetchAll(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }


    //AJOUT
    public function addReservation($nom, $date, $accesibilite) //non nommé
    {
        $db = $this->dbConnect();
        $query = $db->prepare('INSERT INTO Evenements(eventNom, eventDate, eventAccesibilite) VALUES(?, ?, ?)');
        $result = $query->execute(array($nom, $date, $accesibilite));

        return $result;
    }

    public function addReservation2($nom, $date, $accesibilite) //nommé
    {
        $db = $this->dbConnect();
        $query = $db->prepare('INSERT INTO Evenements(eventNom, eventDate, eventAccesibilite) VALUES(:nom, :dat, :acc)');
        
        $query->bindValue(':nom', $EtuNom);
        $query->bindValue(':dat', $EtuPrenom);
        $query->bindValue(':acc', $Etuage);

        return $result;
    }


    //MODIFICAITON
    public function setReservation($nom, $date, $accesibilite)
    {
        $db = $this->dbConnect();
        $query = $db->prepare('UPDATE Evenements SET eventNom = ?, eventDate = ?,  eventAccesibilite = ? WHERE eventCode = ? ');
        $result = $query->execute(array($nom, $date, $accesibilite));

        return $result;
    }


    //SUPRESSION
    public function delReservation($code)
    {
        $db = $this->dbConnect();
        $query = $db->prepare('DELETE FROM Evenements WHERE eventCode = ?;');
        $result = $query->execute(array($nom));

        return $result;
    }
}