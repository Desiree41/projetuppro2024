<!-- INFORMATIONS IMPORTANTES SUR LES FONCTIONS A UTILISER POUR LA PROTECTION -->

<!-- 

htmlspecialchars() enlève l'effet des codes HTML (evite les failles XSS) 

nl2br() permet de conserver les retrours a la ligne quand il s'agit de longs texts (textarea) 

Le égale (=) devant la balise ouvrante du php remplace le "php echo"
Autrement dit : <?= $nom ?> équivaut a <?php echo $nom ?>
Utilisons tous le égal quand il s'agit juste d'afficher une valeur comme dans l'exmple

-->
<!-- a utiliser pour chaque appelation de $row -->
<?= htmlspecialchars($row['nom']) ?> 
<?= nl2br(htmlspecialchars($row['description'])) ?> 

<!-- TEMPLATE BOUCLE -->
<?php if (count($list) > 0) {   foreach ($list as $row) { ?> 

<!-- CONTENU -->

<?php } }?>
<!-- ! TEMPLATE BOUCLE -->



<!-- CONDITION POUR VERIFIER QUE LE RESULTAT N'EST PAS VIDE -->
<?php if (count($list) > 0) {

} ?>

<!-- CONDITIONS POUR VERIFIER QUE LA REQUETE EST BIEN EXECUTEE -->
<?php 

if ($list == true ) {

}
 //OU
if ($list != null ) {

}
