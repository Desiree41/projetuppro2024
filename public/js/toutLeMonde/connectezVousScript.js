const btnDeconnexion = document.querySelector(' #btnDeconnexion');
const modal = document.querySelector('.modal');
const btnConnectezVous = document.querySelector('#btnConnectez-Vous');
const form= document.querySelector('.modal-form');
const closeBtn = document.querySelector('.close');


// Events
btnConnectezVous.addEventListener('click', cadreLogin);
closeBtn.addEventListener('click', closeModal);
// window.addEventListener('click', outsideClick);

btnDeconnexion.addEventListener('click', inverserBoutton2);
// btnConnectezVous.addEventListener('click', inverserBoutton);



// Sortir de la boite modal une fois qu'on clique sur l'écran hors du formulaire
function outsideClick(e) {
  if (e.target == modal) {
    modal.style.display = "none";
  }
}

// Close
function closeModal() {
  modal.style.display = "none";
}

function inverserBoutton()
{
  if (btnConnectezVous.style.display= 'none') 
  {
    btnDeconnexion.style.display = 'inline-block';
    btnConnectezVous.style.display =  'none';
  }
  else
  {
    btnDeconnexion.style.display = 'none';
    btnConnectezVous.style.display =  'inline-block';
  }
}

function inverserBoutton2()
{
  if (btnConnectezVous.style.display= "none") 
  {
    btnDeconnexion.style.display = "inline-block";
    btnConnectezVous.style.display =  "none";
  }
  else
  {
    btnDeconnexion.style.display = "none";
    btnConnectezVous.style.display =  "inline-block";
  }
}

//ANIMATION A L'APPARITION
function cadreLogin()
{
  modal.style.display = "flex";
  modal.style.justifyContent="center";
  // chicMouv();
}


function chicMouv()
{
  let position= 0;

  let id= setInterval(mvt,0.0000000000000000000000000000000001); //id pas necessaire //intervalle pour chaque déplacement
  function mvt()
  {
    if (position>110)
    {
      clearInterval(id);  //on arrête la d'appeler la fonction
    }
    else
    {
      position = position+1; 
      form.style.top= position + "px";
    }
  }
}