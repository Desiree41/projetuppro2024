<?php
require_once('controller/AdministrateurController.php'); //pk once?
require_once('controller/EtudiantController.php'); 
require_once('controller/ToutLeMondeController.php'); 

$toutLeMonde = new ToutLeMonde();
$administrateur = new Administrateur();
$etudiant = new Etudiant();

try {
    if (isset($_GET['action'])) {

        /*ACCEUIL !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        if ($_GET['action'] == 'acceuil') 
        { 
            $toutLeMonde->ouvrirAcceuil();
        }

        /*CONTACT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'contact') {
            if (true) {
                $toutLeMonde->ouvrirContact();
            }
            else {
                throw new Exception('Aucun identifiant de billet envoyé');
            }
        }
        elseif ($_GET['action'] == 'aProposDeNous') {
            if (true) {
                $toutLeMonde->ouvrirAProposDeNous();
            }
            else {
                throw new Exception('Aucun identifiant de billet envoyé');
            }
        }
    }
    else {
        $toutLeMonde->ouvrirAcceuil();
    }
}
catch(Exception $e) {
    echo 'Erreur : ' . $e->getMessage();
}
