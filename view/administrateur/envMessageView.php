<?php $titre = 'Administrateur'; ?>
<?php $style = 'administrateur/envMessageStyle.css'; ?>
<?php $script = ''; ?>

<?php ob_start(); ?>
<!-- SIDEBAR :::::::::::::::::::::::::::::::::::::::::::::-->
<?php require 'view/administrateur/sideBar.php'; ?>
<!-- FIN SIDEBAR $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-->
<div id="root">
    <div class="blocTitle">
        <h1>Message à <span><?= htmlspecialchars($row['EtuNom']) ?><?php echo ' '.htmlspecialchars($row['EtuPrenom']) ?></span></h1>
    </div>
    <div class="main">
        <form action="index.php?action=addMessageFromAdmin&EtuCode=<?= htmlspecialchars($row['EtuCode']) ?>&EventCode=<?= $_GET['EventCode'] ?>" method="POST">
            <div class="contenaireReservation">

                <div class="contenaireChamp">
                    <div class="blocInput hInput">
                        <input type="text" required name="objet">  
                        <span class="input_span">OBJET</span>
                    </div>
                    
                    <div class="blocInput hTextarea">
                        <textarea required name="contenu"></textarea>
                        <span class="textarea_span">MESSAGE</span>
                    </div>
                </div>
            </div>
            <div class="boutouEnvoie">
                <input type="submit" value="Envoyer">
            </div>
        </form>
    </div>
</div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>

