<?php $titre = 'Administrateur'; ?>
<?php $style = 'administrateur/modifierProfilStyle.css'; ?>
<?php $script = ''; ?>

<?php ob_start(); ?>
<!-- SIDEBAR :::::::::::::::::::::::::::::::::::::::::::::-->
<?php require 'view/administrateur/sideBar.php'; ?>
<!-- FIN SIDEBAR $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-->
<div id="root">
    <div class="blocTitle">
        <h1>MON PROFIL</h1>
    </div>
    <div class="main">
        <form action="index.php?action=setProfilAdmin" method="POST">
            <div class="contenaireReservation">

                <div class="contenaireChamp">

                    <div class="blocInput hInput">
                        <input type="text" required name="nom" value="<?= htmlspecialchars($row['AdminNom']) ?>">  
                        <span class="input_span">Nom</span>
                    </div>

                    <div class="blocInput hInput">
                        <input type="text" required name="prenom" value="<?= htmlspecialchars($row['AdminPrenom']) ?>">  
                        <span class="input_span">Prénom</span>
                    </div>

                    <div class="blocInput hInput">
                        <input type="text" required name="email" value="<?= htmlspecialchars($row['AdminEmail']) ?>">  
                        <span class="input_span">Email</span>
                    </div>

                </div>
            </div>
            <div class="boutouEnvoie">
                <input type="submit" value="Modifier">
            </div>
        </form>
    </div>
</div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>