<?php $titre = 'Administrateur'; ?>
<?php $style = 'administrateur/administrateurStyle.css'; ?>
<?php $script = ''; ?>

<?php ob_start(); ?>
<!-- SIDEBAR :::::::::::::::::::::::::::::::::::::::::::::-->
<?php require 'view/administrateur/sideBar.php'; ?>
<!-- FIN SIDEBAR $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-->

<div class="contenuAdmin">
	<h1>BOÎTE DE RÉCEPTION</h1>
	<section class="groupMessage">

		<!-- TEMPLATE BOUCLE -->
		<?php if (count($list) > 0) {   foreach ($list as $row) { ?> 

			<div class="blocMess">
				<div class="dateMess"><div><?= htmlspecialchars($row['DateReserv']) ?></div><div><?= htmlspecialchars($row['HeureReserv']) ?></div></div>
				<div class="message">
					<h2 class="filiereMessage"><?= htmlspecialchars($row['EcoleNom']) ?>/<?= htmlspecialchars($row['FilNom']) ?></h2>
					<nav class="navMessave">
						<ul class="nav__links">
							<li><?= htmlspecialchars($row['EventNom']) ?></li>
							<li><?= htmlspecialchars($row['DateEvent']) ?></li>
							<li>Lieu: <?= htmlspecialchars($row['EspNom']) ?></li>
						</ul>
					</nav>
					<a class="cta" href="index.php?action=envMessage&EtuCode=<?= htmlspecialchars($row['EtuCode']) ?>&EventCode=<?= htmlspecialchars($row['EventCode']) ?>"><button class="btn_envMessage">Message</button></a>
					<a class="cta" href="index.php?action=detailMessageEtu&EventCode=<?= htmlspecialchars($row['EventCode']) ?>"><button class="btn_ouvrirMessage">Ouvrir</button></a>
				</div>
			</div>

		<?php } }?>
		<!-- ! TEMPLATE BOUCLE -->

	</section>
</div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>

