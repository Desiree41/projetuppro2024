<?php $titre = 'Administrateur'; ?>
<?php $style = 'administrateur/detailMessageEtuStyle.css'; ?>
<?php $script = ''; ?>

<?php ob_start(); ?>
<!-- SIDEBAR :::::::::::::::::::::::::::::::::::::::::::::-->
<?php require 'view/administrateur/sideBar.php'; ?>
<!-- FIN SIDEBAR $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-->
<div id="root">
    <div class="blocTitle">
        <h1>Réservation de <span><?= htmlspecialchars($row['EtuNom']) ?><?php echo ' '.htmlspecialchars($row['EtuPrenom']) ?></span></h1>
    </div>
    <div class="main">
        <form action="index.php?action=reponseReservation&EventCode=<?= $_GET['EventCode'] ?>&reponse=1" method="POST">
            <div class="contenaireReservation">
                
                <div class="contenaireChamp">
                    <div class="blocInput hInput">
                        <span class="choixFichier-span">Nom de l'évènement</span>
                        <input Disabled="disabled" type="text" Disabled="disabled"  placeholder="Nom de l'évènement" value="<?= htmlspecialchars($row['EventNom']) ?>">  
                    </div>
                    
                    <div class="blocInput hInput">
                        <span class="choixFichier-span">Date de l'évènement</span>
                        <input Disabled="disabled" type="date" value="<?= htmlspecialchars($row['EventDate']) ?>" >
                    </div>

                    <div class="blocInput hInput">
                        <span class="choixFichier-span">Réservé a</span>
                        <input Disabled="disabled" type="text" placeholder="Réservé à" value="<?= htmlspecialchars($row['EventAccesibilite']) ?>">
                    </div>
                    <div class="blocInput hTextarea">
                        <span class="choixFichier-span">Description</span>
                        <textarea  Disabled="disabled" placeholder="Descritption brève"><?= htmlspecialchars($row['EventDescription']) ?></textarea>
                    </div>
                </div>

                <div class="contenaireBoutton">
                    <div class="choixFichier">
                        <span class="choixFichier-span">Type de l'évènement</span>
                        <input Disabled="disabled" type="text"  value="<?= htmlspecialchars($row['TypeNom']) ?>"><br>
                    </div>
                    
                    <div class="choixFichier">
                        <span class="choixFichier-span">Nom de l'espace</span>
                        <input Disabled="disabled" type="text" value="<?= htmlspecialchars($row['EspNom']) ?>" ><br>
                    </div>

                    <div class="choixFichier" >
                        <span class="choixFichier-span">Catégorie de l'espace</span>
                        <input Disabled="disabled" type="text" value="<?= htmlspecialchars($row['CatLibelle']) ?>" ><br>
                    </div>

                    <div class="choixFichier">
                        <span class="choixFichier-span">Localistion de l'espace</span>
                        <input Disabled="disabled" type="text" value="<?= htmlspecialchars($row['SiteNom']) ?>" ><br>
                    </div>  
                </div>

            </div>
            <div class="boutouEnvoie">
                <input type="submit" value="Accepter" class="boutouEnvoie_accept">
                <a class="boutouEnvoie_refuse" href="index.php?action=reponseReservation&EventCode=<?= $_GET['EventCode'] ?>&reponse=0"><span>Refuser</span></a>
            </div>
        </form>

        <div class="contenaireImage">
            <div class="blocImage">
                <h3>Affiche publicitaire</h3>
                <img class="imageEvent" src="public/image/evenement/<?= $row['EventPhoto']  ?>" alt="Image de l'évènement">
            </div>
        </div>
    </div>
</div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>

