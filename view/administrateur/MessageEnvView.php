<?php $titre = 'Administrateur'; ?>
<?php $style = 'administrateur/MessageEnvStyle.css'; ?>
<?php $script = ''; ?> 

<?php ob_start(); ?>
<!-- SIDEBAR :::::::::::::::::::::::::::::::::::::::::::::-->
<?php require 'view/administrateur/sideBar.php'; ?>
<!-- FIN SIDEBAR $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-->
<div class="contenuAdmin">
	<h1>MESSAGES  ENVOYÉS</h1>
	<section class="groupMessage">

		<!-- TEMPLATE BOUCLE -->
		<?php if (count($list) > 0) {   foreach ($list as $row) { ?> 

			<div class="blocMess">
				<div class="dateMess"><div><?= htmlspecialchars($row['DateMess']) ?></div><div><?= htmlspecialchars($row['HeureMess']) ?></div></div>
				<div class="message">
					<h2 class="filiereMessage"><?= htmlspecialchars($row['EcoleNom']) ?>/<?= htmlspecialchars($row['FilNom']) ?></h2>
					<nav class="navMessave">
						<ul class="nav__links">
							<li><?= htmlspecialchars($row['EventNom']) ?></li>
							<li><?= htmlspecialchars($row['DateEvent']) ?></li>
							<li>Lieu: <?= htmlspecialchars($row['EspNom']) ?></li>
						</ul>
					</nav>
					<a class="cta" href="index.php?action=detailMessageEnv&MessCode=<?= htmlspecialchars($row['MessCode']) ?>&EtuNom=<?= htmlspecialchars($row['EtuNom']) ?>&EtuPrenom=<?= htmlspecialchars($row['EtuPrenom']) ?>"><button class="btn_ouvrirMessage">Voir plus</button></a>
				</div>
			</div>

		<?php } }?>
		<!-- ! TEMPLATE BOUCLE -->

	</section>
</div>
<!-- OPTIION MESSAGE ENVOYES $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-->


<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>

