<header>
	<!-- <img src="public/image/logo/inp.png"> -->
	<strong>
		<span>E</span>ven<span>INP</span> 
	</strong>
	<nav>
		<a href="index.php?action=acceuil">Accueil</a>
		<a href="index.php?action=evenement">Evènement</a>
		<?= $reservation ?>
		<?= $administrateur ?>
		<a href="index.php?action=contact">Contact</a>
		<a href="index.php?action=aProposDeNous">A propos de nous</a>

		<?php if (isset($_SESSION['email'])):?>
			<a href="index.php?action=deconnexion" id="lienDeconnexion" onclick="return confirm('Voulez-vous vraiment vous déconnecter ?')">
				<button id="btnDec	onnexion"  class="animated-button12 deconnexion2">
					Deconnexion
				</button>
			</a>

		<?php else: ?>
			<button id="btnConnectez-Vous" class="animated-button3 ">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
				Connexion
			</button>
			
		<?php endif; ?>
	</nav>

	<style>
		header
		{
			position: absolute;
			top: 0;
			left: 0;		
			display: flex;
			justify-content: space-between;
			align-items: center;
			height: 5em;
			background-color: rgba(0,0,0,0.5);
			width: 100vw;
			margin-bottom: 3px;
			z-index: 1000; /*pour qu'il soit au dessus de tous*/

			/*position: absolute; ainsi n'est pas affecté par le positionnement des autress*/

		}
		header strong
		{
			font-family: "Bauhaus 93";
			font-size: 2.5em;
			color: orange;
		}

		header nav a
		{
			font-size: 1.2em;
			text-decoration: none;
			color: white;
			margin-right: 2em;
			font-family: 'Times New Roman';
		}

		header nav a:hover
		{
			transition: 0.3s;
			color: orange;
			text-decoration: overline;
		}

		header nav #btnConnectez-Vous, #lienDeconnexion
		{
			outline: none;
			margin-right: 1em;
		}
	</style>
	
</header>
