<?php $titre = 'Réservation'; ?>
<?php $style = 'etudiant/boiteDeReceptionEtuStyle.css'; ?>
<?php $script = ''; ?> 

<?php ob_start(); ?>
<!-- SIDEBAR :::::::::::::::::::::::::::::::::::::::::::::-->
<?php require 'view/etudiant/sideBar.php'; ?>
<!-- FIN SIDEBAR $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-->

<div class="contenuAdmin">
	<h1>BOÎTE DE RÉCEPTION</h1>
	<section class="groupMessage">

		<!-- TEMPLATE BOUCLE -->
		<?php if (count($list) > 0) {   foreach ($list as $row) { ?>
			<?php if(htmlspecialchars($row['EventEstAccepte']) == 1): ?>

				<div class="blocMess">
					<div class="dateMess"><div><?= htmlspecialchars($row['DateReserv']) ?></div><div><?= htmlspecialchars($row['HeureReserv']) ?></div></div>
					<div class="message">
						<h2 class="filiereMessage"><?= htmlspecialchars($row['AdminNom']) ?></h2>
						<nav class="navMessave">
							<ul class="nav__links">
								<li><?= htmlspecialchars($row['EventNom']) ?></li>
								<li><?= htmlspecialchars($row['DateEvent']) ?></li>
								<li>Lieu: <?= htmlspecialchars($row['EspNom']) ?></li>
							</ul>
						</nav>
						<a class="cta" href="index.php?action=detailMessageRetour&EventCode=<?= htmlspecialchars($row['EventCode']) ?>">
							<button Style="background: green; width:7.8em;" class="btn_ouvrirMessage">Accepté</button>
						</a>
					</div>
				</div>

			<?php else: ?>

				<div class="blocMess">
					<div class="dateMess"><div><?= htmlspecialchars($row['DateReserv']) ?></div><div><?= htmlspecialchars($row['HeureReserv']) ?></div></div>
					<div class="message">
						<h2 class="filiereMessage"><?= htmlspecialchars($row['AdminNom']) ?></h2>
						<nav class="navMessave">
							<ul class="nav__links">
								<li><?= htmlspecialchars($row['EventNom']) ?></li>
								<li><?= htmlspecialchars($row['DateEvent']) ?></li>
								<li>Lieu: <?= htmlspecialchars($row['EspNom']) ?></li>
							</ul>
						</nav>
						<a class="cta" href="index.php?action=detailMessageRetour&EventCode=<?= htmlspecialchars($row['EventCode']) ?>">
							<button Style="background: red; width:7.8em;" class="btn_ouvrirMessage">Refusé</button>
						</a>
					</div>
				</div>

			<?php endif; ?>
		<?php } }?>
		<!-- ! TEMPLATE BOUCLE -->

	</section>
</div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>

