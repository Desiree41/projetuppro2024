<?php $titre = 'Réservation'; ?>
<?php $style = 'etudiant/detailMessageRecuStyle.css'; ?>
<?php $script = ''; ?>

<?php ob_start(); ?>
<!-- SIDEBAR :::::::::::::::::::::::::::::::::::::::::::::-->
<?php require 'view/etudiant/sideBar.php'; ?>
<!-- FIN SIDEBAR $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-->
<div id="root">
    <div class="blocTitle">
        <h1>Message de<span><?php echo' '.$_GET['AdminNom'] ?> <?php echo ' '.$_GET['AdminPrenom'] ?></span></h1>
    </div>
    <div class="main">
        <form action="" >
            <div class="contenaireReservation">

                <div class="contenaireChamp">
                    <div class="blocInput hInput">
                        <span class="input_span">OBJET</span>
                        <input Disabled="disabled"type="text" required value="<?= htmlspecialchars($row['MessObjet']) ?>">  
                    </div>
                    
                    <div class="blocInput hTextarea">
                        <span class="textarea_span">MESSAGE</span>
                        <textarea Disabled="disabled" required ><?= htmlspecialchars($row['MessContenu']) ?></textarea>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>

