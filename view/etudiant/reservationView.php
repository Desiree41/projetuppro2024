<?php $titre = 'Reservation'; ?>
<?php $style = 'etudiant/reservationStyle.css'; ?>
<?php $script = ''; ?> 

<?php ob_start(); ?>
<!-- SIDEBAR :::::::::::::::::::::::::::::::::::::::::::::-->
<?php require 'view/etudiant/sideBar.php'; ?>
<!-- FIN SIDEBAR $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-->
<div id="root">
    <div>
        <h1>RÉSERVATION</h1>
    </div>
    <div class="main">
        <form action="index.php?action=addReservation" method="POST" enctype="multipart/form-data">
            <div class="contenaireReservation">
                
                <div class="contenaireChamp">
                    <div class="blocInput hInput">
                        <input name="EventNom" type="text" required placeholder="Nom de l'évènement" class="inp" >  
                    </div>
                    
                    <div class="blocInput hInput">
                        <input  name="EventDate" type="date" required class="inp">
                        <!-- <span class="span-date">Date de l'evenement:</span> -->
                    </div>

                    <div class="blocInput hInput">
                        <input name="EventAccess" type="text" placeholder="Réservé à" class="inp">
                    </div>
                    <div class="blocInput hTextarea">
                        <textarea name="EventDes" class="inp" required placeholder="Descritption brève"></textarea>
                    </div>
                </div>

                <div class="contenaireBoutton">
                    <div class="choixFichier">
                        <span class="choixFichier-span inp">Affiche publicitaire</span>
                        <input name="EventPhoto" type="file"><br>
                    </div>

                     <div class="choixFichier">
                        <input name="EspNom" type="text" required placeholder="Nom de l'espace"><br>
                    </div>

                    <div class="bouttonEtCombo">
                        <select class="option inp" name="EventLoc" required >
                            <option value="" selected="disabled" selected="selected" class="titreCombo optHColor" >Localisation</option>
                            <option value="1">INP-SUD</option>
                            <option value="2">INP-CENTRE</option>
                            <option value="3">INP-NORD</option>
                        </select>
                    </div>

                    <!-- <div class="choixFichier">
                        <input name="EventCat" type="text" required placeholder="Catégorie de l'espace"><br>
                    </div> -->

                    <div class="bouttonEtCombo">
                        <select  class="option inp" name="EventCat" required >
                            <option value="" selected="disabled" selected="selected" class="titreCombo optHColor">Catégorie de l'espace</option>
                            <option value="1">Amphithéâtre</option>
                            <option value="2">Gymnase</option>
                            <option value="3">Salle de classe</option>
                            <option value="4">Espace libre</option>
                            <option value="5">Autres</option>
                        </select>
                    </div>


                    <div class="bouttonEtCombo">
                        <select class="option inp" name="EventType" required >
                            <option value="" selected="disabled" selected="selected" class="titreCombo optHColor">Type de l'événement</option>
                            <option value="1">Evenement Sportif</option>
                            <option value="2">Evenement Festif</option>
                            <option value="3">Evenement Educatif</option>
                            <option value="4">Autres</option>
                        </select>
                    </div>



                    
                    <!-- <div class="bouttonEtCombo">
                        <select class="option inp" name="type">
                            <option selected="disabled" selected="selected" class="optHColor">Nom de l'espace</option>
                            <option>Amphi Adou jonas</option>
                            <option>Espace Akwaba</option>
                            <option>Salle303</option>
                            <option>Autres</option>
                        </select>
                    </div>     -->

                </div>

            </div>
            <div class="boutouEnvoie">
                <input type="submit" value="Envoyer">
            </div>
        </form>

        <div class="contenaireImage">
            <div class="blocImage">
                <h3>Espace</h3>
                <img class="imagesEspace" src="public/image/background/admin4.jpg" alt="">
            </div>
            
        </div>
    </div>
</div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>

