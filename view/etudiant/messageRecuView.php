<?php $titre = 'Réservation'; ?>
<?php $style = 'etudiant/reservationEffectueStyle.css'; ?>
<?php $script = ''; ?>

<?php ob_start(); ?>
<!-- SIDEBAR :::::::::::::::::::::::::::::::::::::::::::::-->
<?php require 'view/etudiant/sideBar.php'; ?>
<!-- FIN SIDEBAR $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-->

<div class="contenuAdmin">
	<h1>MESSAGES REÇU</h1>
	<section class="groupMessage">

		<!-- TEMPLATE BOUCLE -->
		<?php if (count($list) > 0) {   foreach ($list as $row) { ?> 

			<div class="blocMess">
				<div class="dateMess"><div><?= htmlspecialchars($row['DateMess']) ?></div><div><?= htmlspecialchars($row['HeureMess']) ?></div></div>
				<div class="message">
					<h2 class="filiereMessage"><?= htmlspecialchars($row['AdminNom']) ?></h2>
					<nav class="navMessave">
						<ul class="nav__links">
							<li><?= htmlspecialchars($row['EventNom']) ?></li>
							<li><?= htmlspecialchars($row['DateEvent']) ?></li>
							<li>Lieu: <?= htmlspecialchars($row['EspNom']) ?></li>
						</ul>
					</nav>
					<a class="cta" href="index.php?action=detailMessageRecu&MessCode=<?= htmlspecialchars($row['MessCode']) ?>&AdminNom=<?= htmlspecialchars($row['AdminNom']) ?>&AdminPrenom=<?= htmlspecialchars($row['AdminPrenom']) ?>"><button class="btn_ouvrirMessage">Voir plus</button></a>
				</div>
			</div>

		<?php } }?>
		<!-- ! TEMPLATE BOUCLE -->

	</section>
</div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>

