<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?= $titre ?></title>
        <link href="public/css/styleButton3.css" rel="stylesheet" />
        <link href="public/css/styleButton2.css" rel="stylesheet" />
        <link href="public/css/styleButton.css" rel="stylesheet" />
        <link href="public/css/styleSideBar3.css" rel="stylesheet" />
        <link href="public/css/styleBarMessage.css" rel="stylesheet" />
        <link href="public/css/footer3.css" rel="stylesheet" />
        <link href="public/css/toutLeMonde/connectezVousStyle.css" rel="stylesheet" />
        <link href="public/css/<?= $style ?>" rel="stylesheet" /> 
    </head>
        
    <body>
        
        <?php require('view/header.php') ?> <!-- ON CHARGE  LE HEADER -->

        <?= $contenu ?> <!-- ON CHARGE  LE CONTENU DE LA PAGE -->

        <!-- BOITE MODAL DE CONNEXION--> 
        <div class="modal">
            <form action="index.php?action=connexion" method="POST" class="modal-form">
                <fieldset class="modal-fieldset">
                    <div>
                        <span class="close">&times;</span>
                    </div>

                    <div class="modal-header">
                        <h3 class="titre">Connectez-vous</h3>
                    </div>

                    <div  class="modal-champs champOne">
                        <label class="modal-label" for="email">Email</label><input name="email" class="modal-input" type="email" id="email" placeholder="prenom.nomNN@inphb.ci" maxlength="60" required >
                    </div>

                    <div class="modal-champs champTwo">
                        <label class="modal-label" for="mdp">Mot de passe</label><input  name="password" class="modal-input" type="password" id="mdp" placeholder="xxxxx" required>
                    </div>

                    <div class="modal-checkbox">
                        <input name="identite" value="etudiant" class="modal-input" type="checkbox" id="check" checked placeholder="xxxxx"><label class="modal-label" for="check">Je suis un étudiant</label>
                    </div>

                    <div class="infoIncorrect" style="color: red; font-size: 15px; display: none;">Login ou mot de passe incorrect</div>

                    <div id="connexion">
                        <input type="submit" value="Connexion" class="btn-gradient orange small" style="border-radius: 1em;"/>
                    </div>
                </fieldset>
            </form>
        </div>

        <script type="text/javascript" src="public/js/toutLeMonde/connectezVousScript.js"></script>
    </body>
</html>
