<footer class="footer-distributed">

      <div class="footer-left">

        <h3>INP-HB<span></span></h3>

        <p class="footer-links">
          <a href="index.php?action=acceuil" class="link-1">Accueil</a>
          
          <a href="index.php?action=evenement">Evènement</a>
        
          <a href="index.php?action=contact">Contact</a>
        
          <a href="index.php?action=aProposDeNous">A propos de nous</a>
        </p>

        <p class="footer-company-name">INP-HB © 1996</p>
      </div>

      <div class="footer-center">

        <div>
          <i><img src="public/image/icone/locaFooter.png" alt="localisation"></i>
          <p><span>Adresse</span>Yamoussoukro, Côte d'Ivoire</p>
        </div><br>

        <div>
          <i><img src="public/image/icone/contactSideBar.png" alt="contact"></i>
          <p>+225 07-98-63-26-18</p>
        </div><br>

        <div>
          <i><img src="public/image/icone/home.png" alt="home"></i>
          <p><a href="inphb.ci">inphb.ci</a></p>
        </div>

      </div>

      <div class="footer-right">

        <p class="footer-company-about">
          <span>A propos de l'INP-HB</span>
          L'INP-HB est une institut d'exelence.
        </p>

        <div class="footer-icons">

          <a href="#"><img src="public/image/icone/facebook.png" alt="facebook"></a>
          <a href="#"><img src="public/image/icone/twitter.png" alt="twitter"></a>
          <a href="#"><img src="public/image/icone/linkedin.png" alt="linkedin"></a>
          <a href="#"><img src="public/image/icone/youtube.png" alt="youtube"></a>

        </div>

      </div>

    </footer>