<?php $titre = 'Contact'; ?>
<?php $style = 'toutLeMonde/contactStyle.css'; ?>
<?php $script = ''; ?>

<?php ob_start(); ?>
	<div id="contenaireContact">

		<div class="contenaireInfoContact contact">
			<div class="infoContact">	
				<h2 class="couleurTextInfo">Info contact</h2>			
				<div class="localisation couleurTextInfo" >
					<span><img src="public/image/icone/localisation.png" alt="icone"></span>
					<div>
						<h4>Localisation</h4>
						<p>
							Yamoussoukro-INPHB
						</p>	
					</div>
				</div>		
				<div class="email couleurTextInfo" >
					<span><img src="public/image/icone/email.png" alt="icone"></span>
					<div>
						<h4>Email</h4>
						<p>
							syntiche.attoh19@inphb.ci
						</p>	
					</div>
				</div>
				<div class="telephone couleurTextInfo" > 
					<span><img src="public/image/icone/call.png" alt="icone"></span>
					<div>
						<h4>Télephone</h4>
						<p>
							 +225 01-43-76-93-23
						</p>	
					</div>
				</div>
				<div class="logoContact couleurTextInfo" >	
						<span><img src="public/image/icone/facebook.png" alt="facebook"></span>	
						<span><img src="public/image/icone/twitter.png" alt="twitter"></span>	
						<span><img src="public/image/icone/instagrame.png" alt="instagrame"></span>	
				</div>	
			</div>
		</div>

		<div class ="contenaireFormulaireContact contact">
			<div class="formulaireContact">	
				<h2>Comment pouvons nous vous aider ?</h2>
				<form action="index.php?action=sendEmail" method="POST">
				<p>
					<div class="formulaireContact-1">
						<div class="blocInput hInput">	
							<input  name="nom" type="text" autofocus required />
							<span>Nom</span>		
						</div>
						<div class="blocInput hInput">
							<input name="prenom" type="text"  required>
							<span>Prénom</span>
						</div>
					</div>

					<div class="formulaireContact-2">
						<div class="blocInput hInput">
							<input name="email" type="email" class="email" required>
							<span>Email</span>
						</div>
						<div class="blocInput hInput">
							<input name= "objet" type="text" required />
							<span>Objet</span>
						</div>
					</div>

					<div class="formulaireContact-3">
						<div class="blocInput hTextarea"> 
							<textarea name="message" required></textarea>
							<span>Ecrivez votre message ici...</span>
						</div>
					</div>

					<div class="boutouEnvoie">
						<input type="submit" value="Envoyer">
					</div>

				</p>
			</form>
			</div>
		</div>
		
	</div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>