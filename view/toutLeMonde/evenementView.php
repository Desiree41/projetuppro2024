<?php $titre = 'Evenement'; ?>
<?php $style = 'toutLeMonde/evenementStyle.css'; ?>
<?php $script = ''; ?>

<link href="public/css/footer.css" rel="stylesheet" />

<?php ob_start(); ?>
<!-- DEFILEMENT D'IMAGES -->
<div class="groupDefileImage">
	<div class="div1"> 
		<div class="opacity">
			<div class="text">
				<h1>Sport</h1> 
				<p>Vivez des évènements sportifs en permanence</p>
			</div>
		</div> 
	</div>
	<div class="div2"> 
		<div class="opacity">
			<div class="text">
				<h1>Conférence</h1>
				<p>Assistez à des conférences enrichissantes avec des panelistes toujours plus séduisants</p> 
			</div>
		</div> 
	</div>
	<div class="div3"> 
		<div class="opacity">
			<div class="text">
				<h1>Divertissement</h1>
				<p>Liberez vous l'esprit en participant à des évènements festif</p> 
			</div> 
		</div>
	</div>
	<div class="div4"> 
		<div class="opacity">
			<div class="text">
				<h1>INPHB</h1> 
				<p>A l'INP-HB, on ne s'ennuit jamais</p>
			</div> 
		</div>
	</div>
	<div class="div1"> 
		<div class="opacity">
			<div class="text">
				<h1>Sport</h1> 
				<p>Vivez des évènements sportifs en permanence</p>
			</div> 
		</div>
	</div>
</div>

<!-- EVENEMENTS -->

<div class="root">
	<div class="contenaireEvents">

		<!-- TEMPLATE BOUCLE -->
		<?php if (count($list) > 0) {   foreach ($list as $row) { ?> 

			<section class="contenaireEvents_section">

				<figure title="cliquez sur l'image pour l'afficher sous sa forme originale">
					<a href="public/image/evenement/<?= $row['EventPhoto']  ?>"><img class="imageEvent" src="public/image/evenement/<?= $row['EventPhoto']  ?>" alt="Image de l'évènement"></a>
					
				</figure>

				<div class="groupInfoEvent">
					<h2 class="h2-space"><?= htmlspecialchars($row['EventNom']) ?></h2>
					<div class="infoEventHorizontal">
						<div class="infoEventVertical">
							<div class="div2"> <h3>Date: <?php echo ' '.htmlspecialchars($row['DateEvent']) ?></h3> </div>
							<div class="div3"> <h3>Accéibilité: <?php echo ' '.htmlspecialchars($row['EventAccesibilite']) ?></h3> </div>
							<div class="div4"> <h3>Type: <?php echo ' '.htmlspecialchars($row['TypeNom']) ?></h3> </div>
						</div>

						<div class="infoEventVertical">
							<div class="div1"> <h3>Lieu: <?php echo ' '.htmlspecialchars($row['EspNom']) ?></h3> </div>
							<div class="div1"> <h3>Catégorie: <?php echo ' '.htmlspecialchars($row['CatLibelle']) ?></h3> </div>
							<div class="div1"> <h3>Localisation: <?php echo ' '.htmlspecialchars($row['SiteNom']) ?></h3> </div>
						</div>
					</div>
					<div class="contenaireDescription"> 
						<h3 class="h3-description">Description: </h3> 
						<p>
							<?php echo ' '.htmlspecialchars($row['EventDescription']) ?>
						</p>
					</div>
				</div>
			</section>

		<?php } }?>
		<!-- ! TEMPLATE BOUCLE -->

	</div>
</div>

	<?php require('view/footer.php') ?>

<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>

