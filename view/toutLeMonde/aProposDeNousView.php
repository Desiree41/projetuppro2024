<?php $titre = 'A propos de nous'; ?>
<?php $style = 'toutLeMonde/aProposDeNousStyle.css'; ?>
<?php $script = ''; ?>

<?php ob_start(); ?>
    <div class="root">
        <div class="contenaireAbout ">
            <div class="blocImage">
                <img class="img1" src="public/image/photoAbout/boomer.jpeg" alt="réalistateur du site" >
                <img class="img2" src="public/image/photoAbout/moi.jpeg" alt="réalistateur du site" >
                <img class="img3" src="public/image/photoAbout/mihan.jpeg" alt="réalistateur du site" >
            </div>
            <h1>A propos de nous</h1>
            <h2>Notre Concept, notre histoire, notre équipe</h2>
            <p class="text">Nous sommes une équipe de trois étudiants 
                venant de L'INPHB en TS INFO2. Notre équipe est composée 
                d'etudiants répondant au nom d'ATTOH SYNTICHE, MIHAN ELISEE, 
                KOMBO MADOU HOPE OTHNIEL. Le but de notre application est de permettre 
                à un étudiant de reserver un espace pour un évènement et d'être sûr de l'avoir 
                en toute sécurité. L'équipe regroupe des membres dynamiques et rigoureux.
            </p>

        </div>
    </div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>