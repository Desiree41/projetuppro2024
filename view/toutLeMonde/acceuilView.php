
<?php $titre = 'EventInp'; ?>
<?php $style = 'toutLeMonde/acceuilStyle.css'; ?>
<?php $script = ''; ?>


<?php ob_start(); ?>
<div id="opacity">
	<div id="corps">
		<p class="p1">Inter-école, conférence et bien plus d'évènements à ne pas manquer.</p>
		<p class="p2">Restez connecté pour ne rien rater, on ne sait jamais.</p>

		<div id="btnContinuezInconnu">
			<a href="index.php?action=evenement" class="btnSlideHover">
				<span>COMMENCER</span>
			</a>
		</div>
	</div>
</div>
<?php $contenu= ob_get_clean(); ?>

<?php require('view/template.php') ?>