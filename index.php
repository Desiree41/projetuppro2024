<?php

session_start();

require_once('controller/AdministrateurController.php');
require_once('controller/EtudiantController.php'); 
require_once('controller/ToutLeMondeController.php'); 

$toutLeMonde = new ToutLeMonde();
$admin = new Administrateur();
$etudiant = new Etudiant();

$redirection="";

try {
    if (isset($_GET['action'])) {
        /*############################# ZONE TOUT LE MONDE #############################*/

        /*TLM - ACCEUIL !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        if ($_GET['action'] == 'acceuil') 
        { 

            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    require 'public/require/etut_session.php';
                    $toutLeMonde->ouvrirAcceuil();
                }
                else
                {
                    require 'public/require/admin_session.php';
                    $toutLeMonde->ouvrirAcceuil();
                }
            }
            else 
            {
                $reservation="";
                $administrateur="";
                $toutLeMonde->ouvrirAcceuil();
            }
        }

        /*TLM - CONTACT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'contact') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    require 'public/require/etut_session.php';
                    $toutLeMonde->ouvrirContact();
                }
                else
                {
                    require 'public/require/admin_session.php';
                    $toutLeMonde->ouvrirContact();
                }
            }
            else 
            {
                $reservation="";
                $administrateur="";
                $toutLeMonde->ouvrirContact();
            }
        }

        /*TLM - A PROPOS DE NOUS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'aProposDeNous') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    require 'public/require/etut_session.php';
                    $toutLeMonde->ouvrirAProposDeNous();
                }
                else
                {
                    require 'public/require/admin_session.php';
                    $toutLeMonde->ouvrirAProposDeNous();
                }
            }
            else 
            {
                $reservation="";
                $administrateur="";
                $toutLeMonde->ouvrirAProposDeNous();
            }
        }

        /*TLM - EVENEMENT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/    
        elseif ($_GET['action'] == 'evenement') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    require 'public/require/etut_session.php';
                    $toutLeMonde->ouvrirEvenement();
                }
                else
                {
                    require 'public/require/admin_session.php';
                    $toutLeMonde->ouvrirEvenement();
                }
            }
            else 
            {
                $reservation="";
                $administrateur="";
                $toutLeMonde->ouvrirEvenement();
            }
        }

        /*TLM - SEND EMAIL !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/    
        elseif ($_GET['action'] == 'sendEmail') 
        {
            if (isset($_POST['nom']) and isset($_POST['prenom']) and isset($_POST['email']) and isset($_POST['objet']) and isset($_POST['message'])) 
            {
                if (!empty($_POST['nom']) and !empty($_POST['prenom']) and !empty($_POST['email']) and !empty($_POST['objet']) and !empty($_POST['message'])) 
                {
                    if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
                    {
                        if ($_SESSION['identite'] === "etudiant") 
                        {
                            require 'public/require/etut_session.php';
                            $toutLeMonde->sendEmail();
                        }
                        else
                        {
                            require 'public/require/admin_session.php';
                            $toutLeMonde->sendEmail();
                        }
                    }
                    else 
                    {
                        $reservation="";
                        $administrateur="";
                        $toutLeMonde->sendEmail();
                    }
                }
                else
                {
                    $redirection="contact";
                    throw new Exception('Veuillez remplir tout les champs');
                }
            }
            else
            {
                $redirection="contact";
                throw new Exception('Vérifiez que toutes données existent !');
            }
            
        }

        /*############################# ZONE ETUDIANT #############################*/

        /*ETUT - RESERVATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/    
        elseif ($_GET['action'] == 'reservation') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    require 'public/require/etut_session.php';
                    $etudiant->ouvrirReservation();
                }
                else
                {
                    $redirection = "administrateur";
                    throw new Exception('Impossible d\'accéder a cette page');
                }
            }
            else 
            {
                $redirection = "acceuil";
                throw new Exception('Votre session a expirée ');
            }
        }



         /*ETUT - BOITE DE RECEPTION ETU !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'boiteDeReceptionEtu') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    require 'public/require/etut_session.php';
                    $etudiant->ouvrirboiteDeReceptionEtu();
                }
                else
                {
                    $redirection = "administrateur";
                    throw new Exception('Impossible d\'accéder a cette page');
                }
            }
            else 
            {
                $redirection = "acceuil";
                throw new Exception('Votre session a expirée ');
            }
        }

        /*ETUT - DETAIL MESSAGE RETOUR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'detailMessageRetour') 
        {
            if (isset($_GET['EventCode']) and $_GET['EventCode'] > 0) 
            {
                if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
                {
                    if ($_SESSION['identite'] === "etudiant") 
                    {
                        require 'public/require/etut_session.php';
                        $etudiant->ouvrirDetailMessageRetour();
                    }
                    else
                    {
                        $redirection = "administrateur";
                        throw new Exception('Impossible d\'accéder a cette page');
                    }
                }
                else 
                {
                    $redirection = "acceuil";
                    throw new Exception('Votre session a expirée ');
                }
            }
            else
            {
                $redirection = "boiteDeReceptionEtu";
                throw new Exception('Aucun évènement n\'a été envoyé !');
            }
            
        }

        /*ETUT - RESERVATION EFFECTUE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'reservationEffectue') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    require 'public/require/etut_session.php';
                    $etudiant->ouvrirReservationEffectue();
                }
                else
                {
                    $redirection = "administrateur";
                    throw new Exception('Impossible d\'accéder a cette page');
                }
            }
            else 
            {
                $redirection = "acceuil";
                throw new Exception('Votre session a expirée ');
            }
        }

        /*ETUT - MESSSAGE RECU !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'messageRecu') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    require 'public/require/etut_session.php';
                    $etudiant->ouvrirMessageRecu();
                }
                else
                {
                    $redirection = "administrateur";
                    throw new Exception('Impossible d\'accéder a cette page');
                }
            }
            else 
            {
                $redirection = "acceuil";
                throw new Exception('Votre session a expirée ');
            }
        }

        /*ETU - DETAIL MESSAGE RECU !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'detailMessageRecu') 
        {
            if (isset($_GET['MessCode']) and $_GET['MessCode'] > 0) 
            {
                if (isset($_GET['AdminNom']) and $_GET['AdminNom'] != "" and isset($_GET['AdminPrenom']) and $_GET['AdminPrenom'] != "") 
                {
                    if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
                    {
                        if ($_SESSION['identite'] === "etudiant") 
                        {
                            require 'public/require/etut_session.php';
                            $etudiant->ouvrirDetailMessageRecu();
                        }
                        else
                        {
                            $redirection = "administrateur";
                            throw new Exception('Impossible d\'accéder a cette page');
                        }
                    }
                    else 
                    {
                        $redirection = "acceuil";
                        throw new Exception('Votre session a expirée ');
                    }
                }
                else
                {
                    $redirection = "messageRecu";
                    throw new Exception('Impossible d\'accéder a cette page');
                }
            }
            else
            {
                $redirection = "messageRecu";
                throw new Exception('Aucun message n\'a été encvoyé');
            }
            
        }


        /*ETU - ADD RESERVATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'addReservation') {
            if (isset($_POST['EventNom'])) {
                if (isset($_POST['EventDate'])) {
                    if (isset($_POST['EventAccess'])) {
                        if (isset($_POST['EventDes'])) {
                            if (isset($_FILES['EventPhoto']) ) {
                                if (isset($_POST['EspNom'])) {
                                    if (isset($_POST['EventLoc'])) {
                                        if (isset($_POST['EventCat'])) {
                                            if (isset($_POST['EventType'])) {
                                                if (!empty($_POST['EventNom']) and !empty($_POST['EventDate']) and !empty($_POST['EventAccess']) and !empty($_POST['EventDes']) and !empty($_POST['EspNom']) and !empty($_POST['EventLoc']) and !empty($_POST['EventCat']) and !empty($_POST['EventType'])) 
                                                {
                                                    if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
                                                    {
                                                        if ($_SESSION['identite'] === "etudiant") 
                                                        {
                                                            require 'public/require/etut_session.php';
                                                            $etudiant->addReservation();
                                                        }
                                                        else
                                                        {
                                                            $redirection = "reservation";
                                                            throw new Exception('Impossible d\'accéder a cette page');
                                                        }
                                                    }
                                                } else {
                                                    $redirection = "reservation";
                                                    throw new Exception(' Remplissez tous les champs !');
                                                }
                                            } else {
                                                $redirection = "reservation";
                                                throw new Exception('Vérifiez que toutes données existent !');
                                            }
                                        } else {
                                            $redirection = "reservation";
                                            throw new Exception('Vérifiez que toutes données existent !');
                                        }
                                    } else {
                                        $redirection = "reservation";
                                        throw new Exception('Vérifiez que toutes données existent !');
                                    }
                                } else {
                                    $redirection = "reservation";
                                    throw new Exception('Vérifiez que toutes données existent !');
                                }
                            } else { $redirection="reservation";
                                throw new Exception('Une est survenue lors de l\'upload du fichier,\n assurez-vous que la taille du fichier ne soit pas trop grande !');
                            }
                        } else {
                            $redirection = "reservation";
                            throw new Exception('Vérifiez que toutes données existent !');
                        }
                    } else {
                        $redirection = "reservation";
                        throw new Exception('Vérifiez que toutes données existent !');
                    }
                } else {
                    $redirection = "reservation";
                    throw new Exception('Vérifiez que toutes données existent !');
                }
            } else {
                $redirection = "reservation";
                throw new Exception('Vérifiez que toutes données existent et assurez-vous que la taille du fichier ne soit pas trop grande !');
            }
        }

        /*############################# ZONE ADMINISTRATEUR #############################*/

        /*ADMIN - ADMINISTRATEUR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/    
        elseif ($_GET['action'] == 'administrateur') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    $redirection = "reservation";
                    throw new Exception('Impossible d\'accéder a cette page');
                }
                else
                {
                    require 'public/require/admin_session.php';
                    $admin->ouvrirAdministrateur();
                }
            }
            else 
            {
                $redirection = "acceuil";
                throw new Exception('Votre session a expirée ');
            }
        }

        /*ADMIN - DETAIL MESSAGE ETU !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'detailMessageEtu') 
        {
            if (isset($_GET['EventCode']) and $_GET['EventCode'] > 0) 
            {
                if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
                {
                    if ($_SESSION['identite'] === "etudiant") 
                    {
                        $redirection = "reservation";
                        throw new Exception('Impossible d\'accéder a cette page');
                    }
                    else
                    {
                        require 'public/require/admin_session.php';
                        $admin->ouvrirDetailMessageEtu();
                    }
                }
                else 
                {
                    $redirection = "acceuil";
                    throw new Exception('Votre session a expirée ');
                }   
            }
            else
            {
                $redirection = "administrateur";
                throw new Exception('Aucun évènement n\'a été envoyé');   
            }
            
        }


        /*ADMIN - MESSAGE ENVOYE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'MessageEnv') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    $redirection = "reservation";
                    throw new Exception('Impossible d\'accéder a cette page');
                }
                else
                {
                    require 'public/require/admin_session.php';
                    $admin->ouvrirMessageEnv();
                }
            }
            else 
            {
                $redirection = "acceuil";
                throw new Exception('Votre session a expirée ');
            }
        }

        /*ADMIN - ENV MESSAGE*/
        elseif ($_GET['action'] == 'envMessage') 
        {

            if (isset($_GET['EtuCode']) and $_GET['EtuCode'] > 0 and isset($_GET['EventCode']) and $_GET['EventCode'] > 0 ) 
            {
                if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
                {
                    if ($_SESSION['identite'] === "etudiant") 
                    {
                        $redirection = "reservation";
                        throw new Exception('Impossible d\'accéder a cette page');
                    }
                    else
                    {
                        require 'public/require/admin_session.php';
                        $admin->ouvrirEnvMessage();
                    }
                }
                else 
                {
                    $redirection = "acceuil";
                    throw new Exception('Votre session a expirée ');
                }   
            }
            else
            {
                $redirection = "administrateur";
                throw new Exception('Aucun évènement n\'a été envoyé');   
            }
            
        }

        /*ADMIN - RETOUR RESERVATION*/
        elseif ($_GET['action'] == 'retourReservation') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    $redirection = "reservation";
                    throw new Exception('Impossible d\'accéder a cette page');
                }
                else
                {
                    require 'public/require/admin_session.php';
                    $admin->ouvrirRetourReservation();
                }
            }
            else 
            {
                $redirection = "acceuil";
                throw new Exception('Votre session a expirée ');
            }
        }

        /*ADMIN - DETAIL MESSAGE REPONDU !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'detailMessageRepondu') 
        {
            if (isset($_GET['EventCode']) and $_GET['EventCode'] > 0) 
            {
                if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
                {
                    if ($_SESSION['identite'] === "etudiant") 
                    {
                        $redirection = "reservation";
                        throw new Exception('Impossible d\'accéder a cette page');
                    }
                    else
                    {
                        require 'public/require/admin_session.php';
                        $admin->ouvrirDetailMessageRepondu();
                    }
                }
                else 
                {
                    $redirection = "acceuil";
                    throw new Exception('Votre session a expirée ');
                }   
            }
            else
            {
                $redirection="retourReservation";
                throw new Exception('Aucun évènement n\'a été envoyé');   
            }
            
        }


       /*ADMIN - ADMINISTRATEUR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/    
        elseif ($_GET['action'] == 'modifierProfilAdmin') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    throw new Exception('Impossible d\'accéder a cette page');
                    header('Location: index.php');
                }
                else
                {
                    require 'public/require/admin_session.php';
                    $admin->ouvrirModifierProfil();
                }
            }
            else 
            {
                $redirection = "acceuil";
                throw new Exception('Votre session a expirée ');
            }
        }

        /*ADMIN - AJOUTER MESSAGE FROM ADMIN !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'addMessageFromAdmin') 
        {
            if (isset($_GET['EtuCode']) and $_GET['EtuCode'] > 0 and isset($_GET['EventCode']) and $_GET['EventCode'] > 0) 
            {
                if (isset($_POST['objet']) and isset($_POST['objet'])) 
                {
                    if (!empty($_POST['objet']) and !empty($_POST['contenu'])) 
                    {
                        if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
                        {
                            if ($_SESSION['identite'] === "etudiant") 
                            {
                                $redirection = "reservation";
                                throw new Exception('Impossible d\'accéder a cette page');
                            }
                            else
                            {
                                require 'public/require/admin_session.php';
                                $admin->addMessageFromAdmin();
                            }
                        }
                        else 
                        {
                            $redirection = "acceuil";
                            throw new Exception('Votre session a expirée ');
                        }  
                    }
                    else
                    {
                        $redirection = "administrateur";
                        throw new Exception('Veuillez remplir le formulaire');
                    }
                }
                else
                {
                    $redirection = "administrateur";
                    throw new Exception('Des donnée n\'existent pas');
                }
            }
            else
            {
                $redirection = "administrateur";
                throw new Exception('Aucun évènement ou étudiant n\'a été envoyé');   
            }
        }

        /*ADMIN - DETAIL MESSAGE ENVOYÉ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
        elseif ($_GET['action'] == 'detailMessageEnv') 
        {
            if (isset($_GET['MessCode']) and $_GET['MessCode'] > 0) 
            {
                if (isset($_GET['EtuNom']) and $_GET['EtuNom'] != "" and isset($_GET['EtuPrenom']) and $_GET['EtuPrenom']) 
                {
                    if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
                    {
                        if ($_SESSION['identite'] === "etudiant") 
                        {
                            $redirection = "reservation";
                            throw new Exception('Impossible d\'accéder a cette page');
                        }
                        else
                        {
                            require 'public/require/admin_session.php';
                            $admin->ouvrirDetailMessageEnv();
                        }
                    }
                    else 
                    {
                        $redirection = "acceuil";
                        throw new Exception('Votre session a expirée ');
                    }   
                }
                else
                {
                    $redirection = "MessageEnv";
                    throw new Exception('Aucun etudiant n\'a été envoyé ');
                }
                
            }
            else
            {
                $redirection = "MessageEnv";
                throw new Exception('Aucun message n\'a été envoyé');   
            }
            
        }

        /*ADMIN - ADMINISTRATEUR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/    
        elseif ($_GET['action'] == 'setProfilAdmin') 
        {
            if (isset($_POST['nom']) and isset($_POST['prenom']) and isset($_POST['email']) ) 
            {
                if (!empty($_POST['nom']) and !empty($_POST['prenom']) and !empty($_POST['email'])) 
                {
                    if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
                    {
                        if ($_SESSION['identite'] === "etudiant") 
                        {
                            $redirection = "reservation";
                            throw new Exception('Impossible d\'accéder a cette page');
                        }
                        else
                        {
                            require 'public/require/admin_session.php';
                            $admin->setProfilAdmin();
                        }
                    }
                    else 
                    {
                        $redirection = "acceuil";
                        throw new Exception('Votre session a expirée ');
                    }
                }
                else
                {
                    $redirection = "administrateur";
                    throw new Exception('Des donnée sont vides');
                }
            }
            else
            {
                $redirection = "administrateur";
                throw new Exception('Aucun valeur n\'a été envoyée');
            }

            
        }
            
        /*ADMIN - RESERVATION ACCEPTEE*/
        elseif ($_GET['action'] == 'reponseReservation') 
        {
            if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    $redirection = "reservation";
                    throw new Exception('Impossible d\'accéder a cette page');
                }
                else
                {
                    if (isset($_GET['EventCode']) and $_GET['EventCode'] > 0) 
                    {
                        if (isset($_GET['reponse']) and ($_GET['reponse'] == 1 or $_GET['reponse'] == 0)) 
                        {
                            require 'public/require/admin_session.php';
                            $admin->setReponseReservation();
                        }
                        else
                        {   $redirection="administrateur";
                            throw new Exception('Aucune réponse n\'a été envoyée');
                        }
                    }
                    else
                    {
                        $redirection = "administrateur";
                        throw new Exception('Aucun évènement n\'a été envoyée');
                    }
                }
            }
            else 
            {
                $redirection = "acceuil";
                throw new Exception('Votre session a expirée ');
            }
        }

        /*CONNEXION*/
        elseif ($_GET['action'] == 'connexion') 
        {
            if (isset($_POST['email']) and isset($_POST['password']) ) 
            {
                if ($_POST['email'] != "" and $_POST['password'] != "") 
                {
                    if (isset($_POST['identite'])) //On recherche dans la table étudiant
                    {
                        $p_identite = "etudiant";
                        require 'public/require/etut_session.php';
                        $tableau = $etudiant->ouvrirReservationC();

                        if ($tableau != null) 
                        {
                            $_SESSION['email'] = $tableau['email'];
                            $_SESSION['password'] = $tableau['password'];
                            $_SESSION['identite'] = $p_identite;
                            header('Location: index.php?action=reservation');
                        }
                    }
                    else //On recherche dans la table adminsitrateur
                    {
                        $p_identite = "admin";
                        require 'public/require/admin_session.php';
                        $tableau = $admin->ouvrirAdministrateurC();

                        if ($tableau != null) 
                        {
                            $_SESSION['email'] = $tableau['email'];
                            $_SESSION['password'] = $tableau['password'];
                            $_SESSION['identite'] = $p_identite;

                            header('Location: index.php?action=administrateur');
                        }
                    }
                }
                else
                {
                    $redirection = "acceuil";
                    throw new Exception('Veuillez remplir tout les champs');
                }
            }
            else 
            {
                $redirection = "acceuil";
                throw new Exception('Vérifiez que toutes données existent !');
            }
        }

        /*GLOBAL - DECONNEXION*/
        elseif ($_GET['action'] == 'deconnexion') 
        {
            header('Location: deconnexion.php');
        }

        else
        {
            $redirection = "acceuil";
            throw new Exception('Cette page n\'existe pas !');
        }

    }
    else 
    {
        if (isset($_SESSION['email']) and isset($_SESSION['password']) and isset($_SESSION['identite'])) 
            {
                if ($_SESSION['identite'] === "etudiant") 
                {
                    require 'public/require/etut_session.php';
                    $toutLeMonde->ouvrirAcceuil();
                }
                else
                {
                    require 'public/require/admin_session.php';
                    $toutLeMonde->ouvrirAcceuil();
                }
            }
            else 
            {
                $reservation="";
                $administrateur="";
                $toutLeMonde->ouvrirAcceuil();
            }
    }
}
catch(Exception $e) {
    echo '<script>alert("Erreur : '.$e->getMessage().'");</script>';
    echo "<script>window.location='index.php?action=$redirection';</script>";
}
