<?php
require_once("model/Connexion.php");

class EtudiantModele extends Connexion
{
	//VERIFICATION D'IDENTIFIANT
	public function checkInfoEtu($email, $password) // Avec insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('SELECT * FROM Etudiant WHERE EtuEmail= ? and EtuMotDePasse= ?');// on prépare la requete
        $query->execute(array($email, $password)); // on execute la requete
        $result = $query->fetch(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    //SELECTION
    public function getReservationRepondu($EtuEmail) // Sans insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('
            SELECT evenements.*, etudiant.*, administrateur.*, espace.*, ecole.*, filiere.*, faire_reservation.*, concerner.*, site.*, categories.*, types.*,
            faire_reservation.EtuCode, faire_reservation.EventCode,  DATE_FORMAT(DateRes, \'%d/%m/%Y\') AS DateReserv, DATE_FORMAT(DateRes, \'%Hh%imin%ss\') AS HeureReserv, DATE_FORMAT(EventDate, \'%d/%m/%Y\') AS DateEvent
            FROM evenements, etudiant, administrateur, espace, ecole, filiere, faire_reservation, concerner, site, categories, types
            WHERE ecole.EcoleCode =filiere.EcoleCode AND filiere.FilCode= etudiant.FilCode 
            AND etudiant.EtuCode = faire_reservation.EtuCode and faire_reservation.EventCode =evenements.EventCode 
            AND evenements.TypeCode = types.TypeCode AND evenements.AdminCode =administrateur.AdminCode
            AND evenements.EventCode = concerner.EventCode AND concerner.EspCode =espace.EspCode 
            AND espace.CatCode = categories.CatCode and espace.SiteCode = site.SiteCode
            AND evenements.EventEstReponseAdmin = 1 and etudiant.EtuEmail= ?
            ORDER BY DateRes DESC
        ');// on execute la requet
        $query->execute(array($EtuEmail));
        $result = $query->fetchAll(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getReservationEffectue($EtuEmail) // Sans insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('
            SELECT evenements.*, etudiant.*, administrateur.*, espace.*, ecole.*, filiere.*, faire_reservation.*, concerner.*, site.*, categories.*, types.*,
            faire_reservation.EtuCode, faire_reservation.EventCode,  DATE_FORMAT(DateRes, \'%d/%m/%Y\') AS DateReserv, DATE_FORMAT(DateRes, \'%Hh%imin%ss\') AS HeureReserv, DATE_FORMAT(EventDate, \'%d/%m/%Y\') AS DateEvent
            FROM evenements, etudiant, administrateur, espace, ecole, filiere, faire_reservation, concerner, site, categories, types
            WHERE ecole.EcoleCode =filiere.EcoleCode AND filiere.FilCode= etudiant.FilCode 
            AND etudiant.EtuCode = faire_reservation.EtuCode and faire_reservation.EventCode =evenements.EventCode 
            AND evenements.TypeCode = types.TypeCode AND evenements.AdminCode =administrateur.AdminCode
            AND evenements.EventCode = concerner.EventCode AND concerner.EspCode =espace.EspCode 
            AND espace.CatCode = categories.CatCode and espace.SiteCode = site.SiteCode
            AND etudiant.EtuEmail= ?
            ORDER BY DateRes DESC
        ');// on execute la requet
        $query->execute(array($EtuEmail));
        $result = $query->fetchAll(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getDetailMessageRetour($codeEvent) // Sans insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('
            SELECT evenements.*, etudiant.*, administrateur.*, espace.*, ecole.*, filiere.*, faire_reservation.*, concerner.*, site.*, categories.*, types.*,
            faire_reservation.EtuCode, faire_reservation.EventCode,  DATE_FORMAT(DateRes, \'%d/%m/%Y\') AS DateReserv, DATE_FORMAT(DateRes, \'%Hh%imin%ss\') AS HeureReserv
            FROM evenements, etudiant, administrateur, espace, ecole, filiere, faire_reservation, concerner, site, categories, types
            WHERE ecole.EcoleCode =filiere.EcoleCode AND filiere.FilCode= etudiant.FilCode 
            AND etudiant.EtuCode = faire_reservation.EtuCode and faire_reservation.EventCode =evenements.EventCode 
            AND evenements.TypeCode = types.TypeCode AND evenements.AdminCode =administrateur.AdminCode
            AND evenements.EventCode = concerner.EventCode AND concerner.EspCode =espace.EspCode 
            AND espace.CatCode = categories.CatCode and espace.SiteCode = site.SiteCode
            AND evenements.EventCode=?
        ');
        $query->execute(array($codeEvent)); // on execute la requete
        $result = $query->fetch(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getMessageRecu($EtuEmail) // Avec insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('
            SELECT message.*,evenements.*, etudiant.*, administrateur.*, espace.*, ecole.*, filiere.*, faire_reservation.*, concerner.*, site.*, categories.*, types.*,
            faire_reservation.EtuCode, faire_reservation.EventCode,  DATE_FORMAT(DateRes, \'%d/%m/%Y\') AS DateReserv, DATE_FORMAT(DateRes, \'%Hh%imin%ss\') AS HeureReserv,
            DATE_FORMAT(MessDate, \'%d/%m/%Y\') AS DateMess, DATE_FORMAT(MessDate, \'%Hh%imin%ss\') AS HeureMess, DATE_FORMAT(EventDate, \'%d/%m/%Y\') AS DateEvent
            FROM message, evenements, etudiant, administrateur, espace, ecole, filiere, faire_reservation, concerner, site, categories, types
            WHERE ecole.EcoleCode =filiere.EcoleCode AND filiere.FilCode= etudiant.FilCode 
            AND etudiant.EtuCode = faire_reservation.EtuCode and faire_reservation.EventCode =evenements.EventCode 
            AND evenements.TypeCode = types.TypeCode AND evenements.AdminCode =administrateur.AdminCode
            AND evenements.EventCode = concerner.EventCode AND concerner.EspCode =espace.EspCode 
            AND espace.CatCode = categories.CatCode and espace.SiteCode = site.SiteCode

            AND Administrateur.AdminCode =Message.AdminCode 
            AND evenements.EventCode = Message.EventCode
            AND etudiant.EtuCode = Message.EtuCode
            AND etudiant.EtuEmail= ?
            ORDER BY DateMess DESC
            ');
        $query->execute(array($EtuEmail)); // on execute la requete
        $result = $query->fetchAll(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getDetailMessageRecu($MessCode) // Avec insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('SELECT * FROM Message WHERE MessCode = ?');// on prépare la requete
        $query->execute(array($MessCode)); // on execute la requete
        $result = $query->fetch(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }


    //ADD RESERSAVTION
    public function addEvent($EventNom, $EventDate, $EventAccesibilite, $EventDescription, $TypeCode, $photo)
    {
        $db = $this->dbConnect();
        $query = $db->prepare('INSERT INTO evenements (EventNom, EventDate, EventAccesibilite, EventDescription, AdminCode, TypeCode, EventPhoto) VALUES( ?, ?, ?, ?, ?, ?, ?)');
        $query->execute([$EventNom, $EventDate, $EventAccesibilite, $EventDescription, 1, $TypeCode, $photo]);
        $result = $query->fetch();

        return $result;
    }

    public function addEspace($EspNom, $site, $CatCode)
    {
        $db = $this->dbConnect();
        $query = $db->prepare('INSERT INTO espace (EspNom,SiteCode,CatCode) VALUES(?,?,?)');
        $query->execute([$EspNom, $site, $CatCode]);
        $result = $query->fetchAll();

        return $result;
    }

    //Recuperation de differents codes EventCode,et autres codes
    public function getEventCode()
    {
        $db = $this->dbConnect();
        $query = $db->query('SELECT  MAX(EventCode)  as  lastEventCode  from evenements');
        $result = $query->fetch();

        return $result;
    }

    //Recuperation du TypeCode
    public function getTypeCode($code)
    {
        $db = $this->dbConnect();
        $query = $db->prepare('SELECT  * from Types WHERE TypeNom = ?');
        $query->execute([$code]);
        $result = $query->fetch();

        return $result;
    }

    public function getCatCode()
    {
        $db = $this->dbConnect();
        $query = $db->query('SELECT  MAX(CatCode)  as lastCatCode  from  Categories');
        $result = $query->fetch();

        return $result;
    }

    public function getEspCode()
    {
        $db = $this->dbConnect();
        $query = $db->query('SELECT  MAX(EspCode)  as lastEspCode  from  espace');
        $result = $query->fetch();

        return $result;
    }

    //Recuperation du TypeCode
    public function getEtuCode($email)
    {
        $db = $this->dbConnect();
        $query = $db->prepare('SELECT * from etudiant WHERE EtuEmail = ?');
        $query->execute(array($email));
        $result = $query->fetch();

        return $result;
    }

    public function AddFaireReservation($Eventcode, $EtuCode)
    {
        $db = $this->dbConnect();
        $query = $db->prepare('INSERT INTO faire_reservation(EventCode,EtuCode, DateRes)VALUES(?,?, NOW())');
        $query->execute([$Eventcode, $EtuCode]);
        $result = $query->fetch();

        return $result;
    }

    public function addConcerner($Eventcode, $EspCode) //non nommé
    {
        $db = $this->dbConnect();
        $query = $db->prepare('INSERT INTO concerner (EventCode,EspCode) VALUES(?,?)');
        $query->execute([$Eventcode, $EspCode]);
        $result = $query->fetch();

        return $result;
    }

}
