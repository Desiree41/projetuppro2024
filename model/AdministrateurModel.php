<?php
require_once("model/Connexion.php");

class AdministrateurModele extends Connexion
{
    //VERIFICATION D'IDENTIFIANT
	public function checkInfoAdmin($email, $password) // Avec insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('SELECT * FROM Administrateur WHERE AdminEmail= ? and AdminMotDePasse= ?');// on prépare la requete
        $query->execute(array($email, $password)); // on execute la requete
        $result = $query->fetch(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

     //SELECTION
    public function getReservationEnCours() // Sans insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->query('
            SELECT evenements.*, etudiant.*, administrateur.*, espace.*, ecole.*, filiere.*, faire_reservation.*, concerner.*, site.*, categories.*, types.*,
            faire_reservation.EtuCode, faire_reservation.EventCode,  DATE_FORMAT(DateRes, \'%d/%m/%Y\') AS DateReserv, DATE_FORMAT(DateRes, \'%Hh%imin%ss\') AS HeureReserv, DATE_FORMAT(EventDate, \'%d/%m/%Y\') AS DateEvent
            FROM evenements, etudiant, administrateur, espace, ecole, filiere, faire_reservation, concerner, site, categories, types
            WHERE ecole.EcoleCode =filiere.EcoleCode AND filiere.FilCode= etudiant.FilCode 
            AND etudiant.EtuCode = faire_reservation.EtuCode and faire_reservation.EventCode =evenements.EventCode 
            AND evenements.TypeCode = types.TypeCode AND evenements.AdminCode =administrateur.AdminCode
            AND evenements.EventCode = concerner.EventCode AND concerner.EspCode =espace.EspCode 
            AND espace.CatCode = categories.CatCode and espace.SiteCode = site.SiteCode
            AND evenements.EventEstReponseAdmin = 0
            ORDER BY DateRes DESC
        ');// on execute la requet
        $result = $query->fetchAll(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getReservationRepondu() // Sans insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->query('
            SELECT evenements.*, etudiant.*, administrateur.*, espace.*, ecole.*, filiere.*, faire_reservation.*, concerner.*, site.*, categories.*, types.*,
            faire_reservation.EtuCode, faire_reservation.EventCode,  DATE_FORMAT(DateRes, \'%d/%m/%Y\') AS DateReserv, DATE_FORMAT(DateRes, \'%Hh%imin%ss\') AS HeureReserv, DATE_FORMAT(EventDate, \'%d/%m/%Y\') AS DateEvent
            FROM evenements, etudiant, administrateur, espace, ecole, filiere, faire_reservation, concerner, site, categories, types
            WHERE ecole.EcoleCode =filiere.EcoleCode AND filiere.FilCode= etudiant.FilCode 
            AND etudiant.EtuCode = faire_reservation.EtuCode and faire_reservation.EventCode =evenements.EventCode 
            AND evenements.TypeCode = types.TypeCode AND evenements.AdminCode =administrateur.AdminCode
            AND evenements.EventCode = concerner.EventCode AND concerner.EspCode =espace.EspCode 
            AND espace.CatCode = categories.CatCode and espace.SiteCode = site.SiteCode
            AND evenements.EventEstReponseAdmin = 1
            ORDER BY DateRes DESC
        ');// on execute la requet
        $result = $query->fetchAll(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getDetailMessageEtu($codeEvent) // Sans insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('
            SELECT evenements.*, etudiant.*, administrateur.*, espace.*, ecole.*, filiere.*, faire_reservation.*, concerner.*, site.*, categories.*, types.*,
            faire_reservation.EtuCode, faire_reservation.EventCode,  DATE_FORMAT(DateRes, \'%d/%m/%Y\') AS DateReserv, DATE_FORMAT(DateRes, \'%Hh%imin%ss\') AS HeureReserv
            FROM evenements, etudiant, administrateur, espace, ecole, filiere, faire_reservation, concerner, site, categories, types
            WHERE ecole.EcoleCode =filiere.EcoleCode AND filiere.FilCode= etudiant.FilCode 
            AND etudiant.EtuCode = faire_reservation.EtuCode and faire_reservation.EventCode =evenements.EventCode 
            AND evenements.TypeCode = types.TypeCode AND evenements.AdminCode =administrateur.AdminCode
            AND evenements.EventCode = concerner.EventCode AND concerner.EspCode =espace.EspCode 
            AND espace.CatCode = categories.CatCode and espace.SiteCode = site.SiteCode
            AND evenements.EventCode=?
        ');
        $query->execute(array($codeEvent)); // on execute la requete
        $result = $query->fetch(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getInfoEtu($codeEtu) // Sans insertion de données
    {
        $db = $this->dbConnect();
        $query = $db->prepare('SELECT * from etudiant WHERE EtuCode = ?');
        $query->execute(array($codeEtu));
        $result = $query->fetch();

        return $result;
    }

    public function getDetailMessageRepondu($codeEvent) // Sans insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('
            SELECT evenements.*, etudiant.*, administrateur.*, espace.*, ecole.*, filiere.*, faire_reservation.*, concerner.*, site.*, categories.*, types.*,
            faire_reservation.EtuCode, faire_reservation.EventCode,  DATE_FORMAT(DateRes, \'%d/%m/%Y\') AS DateReserv, DATE_FORMAT(DateRes, \'%Hh%imin%ss\') AS HeureReserv
            FROM evenements, etudiant, administrateur, espace, ecole, filiere, faire_reservation, concerner, site, categories, types
            WHERE ecole.EcoleCode =filiere.EcoleCode AND filiere.FilCode= etudiant.FilCode 
            AND etudiant.EtuCode = faire_reservation.EtuCode and faire_reservation.EventCode =evenements.EventCode 
            AND evenements.TypeCode = types.TypeCode AND evenements.AdminCode =administrateur.AdminCode
            AND evenements.EventCode = concerner.EventCode AND concerner.EspCode =espace.EspCode 
            AND espace.CatCode = categories.CatCode and espace.SiteCode = site.SiteCode
            AND evenements.EventCode=?
        ');
        $query->execute(array($codeEvent)); // on execute la requete
        $result = $query->fetch(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getInfoAdmin($email) // Avec insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('SELECT * FROM Administrateur WHERE AdminEmail = ?');// on prépare la requete
        $query->execute(array($email)); // on execute la requete
        $result = $query->fetch(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    

    public function getMessageEnv() // Avec insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->query('
            SELECT message.*,evenements.*, etudiant.*, administrateur.*, espace.*, ecole.*, filiere.*, faire_reservation.*, concerner.*, site.*, categories.*, types.*,
            faire_reservation.EtuCode, faire_reservation.EventCode,  DATE_FORMAT(DateRes, \'%d/%m/%Y\') AS DateReserv, DATE_FORMAT(DateRes, \'%Hh%imin%ss\') AS HeureReserv, 
            DATE_FORMAT(MessDate, \'%d/%m/%Y\') AS DateMess, DATE_FORMAT(MessDate, \'%Hh%imin%ss\') AS HeureMess, DATE_FORMAT(EventDate, \'%d/%m/%Y\') AS DateEvent
            FROM message, evenements, etudiant, administrateur, espace, ecole, filiere, faire_reservation, concerner, site, categories, types
            WHERE ecole.EcoleCode =filiere.EcoleCode AND filiere.FilCode= etudiant.FilCode 
            AND etudiant.EtuCode = faire_reservation.EtuCode and faire_reservation.EventCode =evenements.EventCode 
            AND evenements.TypeCode = types.TypeCode AND evenements.AdminCode =administrateur.AdminCode
            AND evenements.EventCode = concerner.EventCode AND concerner.EspCode =espace.EspCode 
            AND espace.CatCode = categories.CatCode and espace.SiteCode = site.SiteCode

            AND Administrateur.AdminCode =Message.AdminCode 
            AND evenements.EventCode = Message.EventCode
            AND etudiant.EtuCode = Message.EtuCode
            ORDER BY DateMess DESC
            ');// on execute la requete
        $result = $query->fetchAll(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    public function getDetailMessageEnv($MessCode) // Avec insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->prepare('SELECT * FROM Message WHERE MessCode = ?');// on prépare la requete
        $query->execute(array($MessCode)); // on execute la requete
        $result = $query->fetch(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }

    //MODIFICAITON
    public function setReponseEvenement($EventCode, $reponse)
    {
        $db = $this->dbConnect();
        $query = $db->prepare('UPDATE Evenements SET EventEstAccepte = ?, EventEstReponseAdmin = 1 WHERE eventCode = ? ');
        $result = $query->execute(array($reponse, $EventCode));

        return $result;
    }

    public function setProfilAdmin($nom, $prenom, $emailP, $emailS)
    {
        $db = $this->dbConnect();
        $query = $db->prepare('UPDATE Administrateur SET AdminNom = ?, AdminPrenom = ?,  AdminEmail = ? WHERE AdminEmail = ? ');
        $result = $query->execute(array($nom, $prenom, $emailP, $emailS));

        return $result;
    }

    //AJOUT
    public function addMessageAdmin($EtuCode, $EventCode, $objet, $contenu) //non nommé
    {

        $db = $this->dbConnect();
        $query = $db->prepare('INSERT INTO Message(EtuCode, EventCode, MessObjet, MessContenu, AdminCode, MessDate) VALUES(?, ?, ?, ?, 1, NOW())');
        $result = $query->execute(array($EtuCode, $EventCode, $objet, $contenu));

        return $result;
    }
}
