<?php
require_once("model/Connexion.php");

class ToutLeMondeModele extends Connexion
{
	public function getEvent() // Sans insertion de données
    {
        $db = $this->dbConnect(); //on se connecte a la bd
        $query = $db->query('
            SELECT evenements.*, etudiant.*, administrateur.*, espace.*, ecole.*, filiere.*, faire_reservation.*, concerner.*, site.*, categories.*, types.*,
            faire_reservation.EtuCode, faire_reservation.EventCode,  DATE_FORMAT(DateRes, \'%d/%m/%Y\') AS DateReserv, DATE_FORMAT(DateRes, \'%Hh%imin%ss\') AS HeureReserv, DATE_FORMAT(EventDate, \'%d/%m/%Y\') AS DateEvent
            FROM evenements, etudiant, administrateur, espace, ecole, filiere, faire_reservation, concerner, site, categories, types
            WHERE ecole.EcoleCode =filiere.EcoleCode AND filiere.FilCode= etudiant.FilCode 
            AND etudiant.EtuCode = faire_reservation.EtuCode and faire_reservation.EventCode =evenements.EventCode 
            AND evenements.TypeCode = types.TypeCode AND evenements.AdminCode =administrateur.AdminCode
            AND evenements.EventCode = concerner.EventCode AND concerner.EspCode = espace.EspCode 
            AND espace.CatCode = categories.CatCode and espace.SiteCode = site.SiteCode
            AND EventEstReponseAdmin = 1 AND EventEstAccepte = 1
            ORDER BY EventDate ASC
        ');// on execute la requet
        $result = $query->fetchAll(); // on met le résultat dans sun tableau

        return $result; //enfin on retourne le resultat
    }
}
