<?php

class Connexion
{
    protected function dbConnect()
    {
        $db = new PDO('mysql:host=localhost;dbname=gesevent;charset=utf8', 'root', '',
            array(
                    PDO::ATTR_EMULATE_PREPARES => false, // évite les injections SQL
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION) );
        return $db;
    }
}
